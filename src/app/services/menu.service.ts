import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class MenuService {
  toggleChanged = new Subject<boolean>();

  setToggle(value: boolean) {
    this.toggleChanged.next(value);
  }
}