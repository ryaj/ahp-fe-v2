import { TestBed } from '@angular/core/testing';

import { ContainerDaoService } from './container-dao.service';

describe('ContainerDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContainerDaoService = TestBed.get(ContainerDaoService);
    expect(service).toBeTruthy();
  });
});
