import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CriteriaService {
  criterias: string[] = [];
  private criteiaSubject: Subject<string[]>;

  constructor() {
    this.criteiaSubject = new Subject();
  }

  initCriteria(criterias: string[]) {
    if (criterias === undefined)
      this.criterias = [];
    else
      this.criterias = criterias;
    this.criteiaSubject.next(this.criterias)
  }

  add(value: string) {
    this.criterias.push(value);
    this.criteiaSubject.next(this.criterias);
  }

  update(idx:number, value: string){
    this.criterias[idx] = value;
    this.criteiaSubject.next(this.criterias);
  }

  deleteById(idx: number){
    this.criterias.splice(idx, 1);
    this.criteiaSubject.next(this.criterias);
  }

  getCriteriaSubj() {
    return this.criteiaSubject
  }
}