import { AhpContainer } from 'src/app/domain/AhpContainer';
import { AlternativeService } from './alternative-service.service';
import { CriteriaService } from './criteria-service.service';
import { Injectable } from '@angular/core';
import { ContainerDTO } from '../domain/ContainerDTO';
import { Observable, Subject } from 'rxjs';
import { ContainerDao } from './container-dao.service';

@Injectable({
  providedIn: 'root'
})
export class AhpService {
  private cachedContainers: Map<string, ContainerDTO>;
  private containerSubject: Subject<ContainerDTO>;

  constructor(
    private containerDao: ContainerDao,
    private criteriaService: CriteriaService,
    private alternativeService: AlternativeService
  ) {
    this.containerSubject = new Subject();
  }

  fetchContainerById(uuid: string) {
    this.containerDao.fetchByUUId(uuid)
      .subscribe(resp => {
        console.log(JSON.stringify(resp));
        this.containerSubject.next(resp);
        this.criteriaService.initCriteria(resp.criterias);
        this.alternativeService.initAlternatives(resp.alternatives);
      });
  }

  createContainer(goal: string) {
    this.containerDao.create(new ContainerDTO(goal))
      .subscribe(resp => {
        console.log(JSON.stringify(resp));
        this.containerSubject.next(resp);
        this.criteriaService.initCriteria(resp.criterias);
        this.alternativeService.initAlternatives(resp.alternatives);
      });
  }

  updateContainer(container: ContainerDTO){
    container.criterias = this.criteriaService.criterias;
    container.alternatives = this.alternativeService.alternativeList;
    this.containerDao.update(container).
    subscribe(resp => {
      console.log(JSON.stringify(resp));
      this.containerSubject.next(resp);
    });
  }

  getContainerSubj(): Observable<ContainerDTO> {
    return this.containerSubject;
  }
}