import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlternativeService {
  alternativeList: string[] = [];
  private alternativeSubj: Subject<string[]>;

  constructor() {
    this.alternativeSubj = new Subject();
  }

  initAlternatives(alternativeList: string[]) {
    if (alternativeList === undefined)
      this.alternativeList = [];
    else
      this.alternativeList = alternativeList;
    this.alternativeSubj.next(this.alternativeList)
  }

  add(value: string) {
    this.alternativeList.push(value);
    this.alternativeSubj.next(this.alternativeList);
  }

  update(idx: number, value: string) {
    this.alternativeList[idx] = value;
    this.alternativeSubj.next(this.alternativeList);
  }

  deleteById(idx: number) {
    this.alternativeList.splice(idx, 1);
    this.alternativeSubj.next(this.alternativeList);
  }

  getAlternativeSubj() {
    return this.alternativeSubj
  }
}