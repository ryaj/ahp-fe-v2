import { TestBed } from '@angular/core/testing';

import { AhpService } from './ahp-service.service';

describe('AhpServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AhpService = TestBed.get(AhpService);
    expect(service).toBeTruthy();
  });
});
