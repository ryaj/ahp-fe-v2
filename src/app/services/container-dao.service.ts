import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ContainerDTO } from '../domain/ContainerDTO';

@Injectable({
  providedIn: 'root'
})
export class ContainerDao {
  private apiUrl = 'http://localhost:9999/container/';

  constructor(private http: HttpClient) { }

  fetchAll(): Observable<ContainerDTO[]> {
    return this.http.get<ContainerDTO[]>(this.apiUrl);
  }

  fetchByUUId(uuid: string): Observable<ContainerDTO> {
    return this.http.get<ContainerDTO>(this.apiUrl + uuid);
  }

  create(container: ContainerDTO): Observable<ContainerDTO> {
    return this.http.post<ContainerDTO>(this.apiUrl, container);
  }

  update(container: ContainerDTO): Observable<ContainerDTO> {
    return this.http.put<ContainerDTO>(this.apiUrl, container);
  }
}
