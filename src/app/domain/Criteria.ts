export class Criteria {
    key: string;
    name: string;

    constructor(name: string) {
        this.name = name;
    }
}
