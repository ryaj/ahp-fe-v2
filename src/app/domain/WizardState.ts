import { WizardStepsEnum } from './WizardStepsEnum';

export class WizardState {
    goal: string;
    step: WizardStepsEnum;
}
