export enum WizardStepsEnum {
    START,
    PARAM_WIZARD,
    CRITERIA_PCM,
    ALT_WIZARD,
    RESULT
}
