import { WizardStepsEnum } from './WizardStepsEnum';

export class AhpContainer {
    goal: string;
    step: WizardStepsEnum;
    criterias: string[];
    criteriasPcm: number[];
    alternatives: string[];
    alternativeByCriteriasPcmMap: [];

    constructor(goal: string) {
        this.goal = goal;
        this.step = WizardStepsEnum.START;
    }
}
