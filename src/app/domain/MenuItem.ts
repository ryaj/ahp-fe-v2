export class MenuItem {
    constructor(
        public type?: string,
        public name?: string,
        public state?: string,
        public icon?: string,
        public active?: boolean,
        public subMenu?: MenuItem[],
    ) { }
}
