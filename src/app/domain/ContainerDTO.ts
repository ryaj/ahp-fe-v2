import { PairedComparisonsMatrix } from './PairedComparisonsMatrix';
export class ContainerDTO {
    containerId: string;
    goal: string;
    step: string;
    criterias: string[];
    alternatives: string[];
    criteriasPcm: PairedComparisonsMatrix;
    alternativeByCriteriasPcmMap: Map<string, PairedComparisonsMatrix>;

    constructor(goal: string) {
        this.goal = goal;
    }
}
