import { MenuService } from './../../services/menu.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  showMenu = true;

  constructor(private menuService: MenuService) { }

  onToggleMenu() {
    this.showMenu = !this.showMenu;
    this.menuService.setToggle(this.showMenu);
  }
}
