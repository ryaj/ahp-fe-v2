import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  footer = '';

  constructor() {
  }

  ngOnInit() {
    const year = new Date().getFullYear();
    this.footer = '© ' + year + ' IStoliarchuk';
  }

}
