import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit, OnDestroy {
  private menuSubs: Subscription;
  menuOpened = true;

  constructor() {
  }

  ngOnInit() { }

  ngOnDestroy() { }
}
