import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from '../app-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContentComponent } from './content/content.component';
import { MenuComponent } from './menu/menu.component';
import { MenuService } from '../services/menu.service';
import { MatIconModule, MatButtonModule } from '@angular/material';

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        ContentComponent,
        MenuComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        MatIconModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        MenuService
    ],
    exports: [AppRoutingModule]
})
export class CoreModule {
}
