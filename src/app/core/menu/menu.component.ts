import { Router } from '@angular/router';
import { MenuItem } from './../../domain/MenuItem';
import { MenuService } from './../../services/menu.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, OnDestroy {
  menuSubs: Subscription;
  showMenu = true;
  menu: MenuItem[] = [
    new MenuItem('link', 'New Goal', '/', 'add_circle_outline', false, []),
  ];
  constructor(
    private router: Router,
    private menuService: MenuService
  ) {
  }

  ngOnInit() {
    this.menuSubs = this.menuService.toggleChanged.subscribe(opened => this.showMenu = opened);
  }

  onMenu(menuItem: MenuItem) {
    this.router.navigate([menuItem.state]);
  }

  ngOnDestroy() {
    this.menuSubs.unsubscribe()
  }

}
