import { WizardComponent } from './wizard.component';
import { AlternativeListComponent } from './alternative-list/alternative-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CriteriaListComponent } from './criteria-list/criteria-list.component';
import { ContainerInfoComponent } from './container-info/container-info.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WizardRoutes } from './wizard.routing';
import { InitStepComponent } from './init-step/init-step.component';
import { RouterModule } from '@angular/router';
import { MatCardModule, MatDividerModule, MatTableModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule, MatListModule, MatMenuModule, MatDialogModule, MatStepperModule } from '@angular/material';
import { NgxLoadingModule } from 'ngx-loading';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { EntityEditComponent } from './entity-edit/entity-edit.component';

@NgModule({
  declarations: [
    InitStepComponent,
    ContainerInfoComponent,
    CriteriaListComponent,
    AlternativeListComponent,
    EntityEditComponent,
    WizardComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatListModule,
    MatDialogModule,
    MatDividerModule,
    MatTableModule,
    MatStepperModule,
    NgxLoadingModule,
    ReactiveFormsModule,
    RouterModule.forChild(WizardRoutes),
  ],
  entryComponents: [
    EntityEditComponent
  ]
})
export class WizardModule { }
