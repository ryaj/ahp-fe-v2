import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { CriteriaService } from 'src/app/services/criteria-service.service';
import { EntityEditComponent } from '../entity-edit/entity-edit.component';

@Component({
  selector: 'app-criteria-list',
  templateUrl: './criteria-list.component.html',
  styleUrls: ['./criteria-list.component.css']
})
export class CriteriaListComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public criteriaList: string[];
  public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  private criteriaSubs: Subscription;

  constructor(
    private criteriaService: CriteriaService,
    public dialog: MatDialog
  ) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.criteriaSubs = this.criteriaService.getCriteriaSubj().subscribe(resp => {
      this.criteriaList = resp;
      this.isLoading = false;
    })
  }

  ngOnDestroy() {
    this.criteriaSubs.unsubscribe();
  }

  onAdd() {
    this.isLoading = true;
    const val = 'new_criteria';
    this.criteriaService.add(val);
  }

  onDelete(idx: number) {
    this.isLoading = true;
    this.criteriaService.deleteById(idx);
  }

  update(idx: number, criteria: string) {
    if (criteria == null) { return; }
    this.isLoading = true;
    this.criteriaService.update(idx, criteria);
  }

  openDialog(idx: number, initCriteria: string) {
    const dialogRef = this.dialog.open(EntityEditComponent, {
      width: '250px',
      data: { idx: idx, value: initCriteria, entityName: 'criteria' }
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log('The dialog was closed');
      if (data) { 
        this.update(data.idx, data.value) 
      }
    });
  }

}