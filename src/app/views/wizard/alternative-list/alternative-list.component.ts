import { AlternativeService } from './../../../services/alternative-service.service';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { EntityEditComponent } from '../entity-edit/entity-edit.component';

@Component({
  selector: 'app-alternative-list',
  templateUrl: './alternative-list.component.html',
  styleUrls: ['./alternative-list.component.css']
})
export class AlternativeListComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public alternativeList: string[];
  public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  private alternativeSubs: Subscription;

  constructor(
    private alternativeService: AlternativeService,
    public dialog: MatDialog
  ) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.alternativeSubs = this.alternativeService.getAlternativeSubj().subscribe(resp => {
      this.alternativeList = resp;
      this.isLoading = false;
    })
  }

  ngOnDestroy() {
    this.alternativeSubs.unsubscribe();
  }

  onAdd() {
    this.isLoading = true;
    const val = 'new_alternative';
    this.alternativeService.add(val);
  }

  onDelete(idx: number) {
    this.isLoading = true;
    this.alternativeService.deleteById(idx);
  }

  update(idx: number, criteria: string) {
    if (criteria == null) { return; }
    this.isLoading = true;
    this.alternativeService.update(idx, criteria);
  }

  openDialog(idx: number, initAlt: string) {
    const dialogRef = this.dialog.open(EntityEditComponent, {
      width: '250px',
      data: { idx: idx, value: initAlt, entityName: 'alternative' }
    });

    dialogRef.afterClosed().subscribe(data => {
      console.log('The dialog was closed');
      if (data) {
        this.update(data.idx, data.value)
      }
    });
  }
}