import { AhpService } from './../../../services/ahp-service.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ngxLoadingAnimationTypes } from 'ngx-loading';

@Component({
  selector: 'app-container-info',
  templateUrl: './container-info.component.html',
  styleUrls: ['./container-info.component.css']
})
export class ContainerInfoComponent implements OnInit, OnDestroy {
  public containerId: string;
  public goal: string;
  public step: string;
  public isLoading: boolean;
  public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  private subscription: Subscription;

  constructor(private ahpService: AhpService) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.subscription = this.ahpService.getContainerSubj().subscribe(resp => {
      console.log("uuid: " + resp.containerId);
      this.goal = resp.goal;
      this.containerId = resp.containerId;
      this.step = resp.step;
      this.isLoading = false;
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}