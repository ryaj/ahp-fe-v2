import { WizardComponent } from './wizard.component';
import { Routes } from '@angular/router';

export const WizardRoutes: Routes = [
  {
    path: '',
    component: WizardComponent
  }
];