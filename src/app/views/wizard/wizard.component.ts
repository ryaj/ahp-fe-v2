import { AhpService } from 'src/app/services/ahp-service.service';
import { ContainerDTO } from './../../domain/ContainerDTO';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnInit, OnDestroy {

  private container: ContainerDTO
  private containerSubs: Subscription;

  constructor(private ahpService: AhpService) { }

  ngOnInit() {
    this.containerSubs = this.ahpService.getContainerSubj()
      .subscribe(resp => {
        console.log("uuid: " + resp.containerId);
        this.container = resp;
      })
  }

  ngOnDestroy() {
    this.containerSubs.unsubscribe();
  }

  save() {
    console.log(JSON.stringify(this.container))
    this.ahpService.updateContainer(this.container);
  }
}
