import { FormGroup, FormControl } from '@angular/forms';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface EditData {
    idx: number;
    value: string;
    entityName: string;
}

@Component({
    selector: 'app-entity-edit',
    templateUrl: './entity-edit.component.html',
    styleUrls: ['entity-edit.component.scss']
})
export class EntityEditComponent {

    alternative: string;
    data: EditData;
    editForm: FormGroup;

    constructor(
        private dialogRef: MatDialogRef<EntityEditComponent>,
        @Inject(MAT_DIALOG_DATA) data: EditData,
    ) {
        this.editForm = new FormGroup({
            value: new FormControl(data.value)
        });
        this.data = data;
        this.alternative = data.value;
    }

    onSave() {
        const newValue= this.editForm.value.value;
        this.dialogRef.close({
            idx: this.data.idx,
            value: newValue
        });
    }

    onClose() {
        this.dialogRef.close();
    }
}