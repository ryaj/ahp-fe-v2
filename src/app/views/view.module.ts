import { MatInputModule, MatButtonModule } from '@angular/material';
import { GoalStepComponent } from './goal-step/goal-step.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
    declarations: [
        GoalStepComponent,
        PageNotFoundComponent
    ],
    imports: [
        NgxLoadingModule,
        MatInputModule,
        MatButtonModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [],
    exports: []
})
export class ViewModule { }