import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AhpContainer } from 'src/app/domain/AhpContainer';
import { Router } from '@angular/router';
import { AhpService } from 'src/app/services/ahp-service.service';
import { ContainerDTO } from 'src/app/domain/ContainerDTO';

@Component({
  selector: 'app-goal-step',
  templateUrl: './goal-step.component.html',
  styleUrls: ['./goal-step.component.css']
})
export class GoalStepComponent implements OnInit {
  goalFormGroup = new FormGroup({
    newGoal: new FormControl(),
    searchId: new FormControl()
  });
  ahpContainer: AhpContainer;
  containerList: ContainerDTO[];
  newContainer: ContainerDTO;

  constructor(
    private router: Router,
    private ahpService: AhpService
  ) { }

  ngOnInit() { }

  onCreateGoal() {
    const newGoal = this.goalFormGroup.get('newGoal').value;
    this.ahpService.createContainer(newGoal);
    this.router.navigate(['/wizard']);
  }

  onSearchContainer() {
    const searchId = this.goalFormGroup.get('searchId').value;
    this.ahpService.fetchContainerById(searchId);
    this.router.navigate(['/wizard']);
  }
}
