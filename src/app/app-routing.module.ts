import { ContentComponent } from './core/content/content.component';
import { GoalStepComponent } from './views/goal-step/goal-step.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/goal',
    pathMatch: 'full'
  },
  {
    path: '',
    component: ContentComponent,
    children: [
      {
        path: 'goal',
        component: GoalStepComponent
      },
      {
        path: 'wizard',
        loadChildren: './views/wizard/wizard.module#WizardModule'
      }
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
