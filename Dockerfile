FROM node:10.14.1 AS builder

WORKDIR /app
ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY . /app
RUN ls
RUN npm i --verbose && \
    npm run build --verbose

FROM nginx:alpine

COPY --from=builder /app/dist/* /usr/share/nginx/html/